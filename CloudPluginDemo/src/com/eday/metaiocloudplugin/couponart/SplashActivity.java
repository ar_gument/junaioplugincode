package com.eday.metaiocloudplugin.couponart;

import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.metaio.cloud.plugin.MetaioCloudPlugin;
import com.metaio.sdk.jni.IMetaioSDKAndroid;

public class SplashActivity extends Activity {
	static {
		IMetaioSDKAndroid.loadNativeLibs();
	}

	/**
	 * standard tag used for all the debug messages
	 */
	public static final String TAG = "MetaioCloudPluginTemplate";

	/**
	 * Display log messages with debug priority
	 * 
	 * @param msg Message to display
	 * @see Log#d(String, String)
	 */
	public static void log(String msg) {
		if (msg != null)
			Log.d(TAG, msg);

	}

	/**
	 * Progress dialog
	 */
	private ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splashscreen);

		JunaioStarterTask junaioStarter = new JunaioStarterTask();
		junaioStarter.execute(1);
		
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	/**
	 * Launch junaio live view
	 */
	private void launchLiveView() {
		// Set your channel id in /res/values/channelid.xml
		int myChannelId = 0;
		
		if(Locale.getDefault().getDisplayLanguage() == "suomi")
			myChannelId = getResources().getInteger(R.integer.channelid);
		else
			myChannelId = getResources().getInteger(R.integer.channelid_suomi);

		// if you have set a channel ID, then load it directly
		if (myChannelId != -1) {
			startChannel(myChannelId, true);
		}
/*		View wmview = getLayoutInflater().inflate(R.layout.watermark, null);
		addContentView(wmview,  new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));		
*/
	}

	public void startAREL(View v) {
		//startChannel(174470, false);// start AREL test
		launchLiveView();
	}
	

	public void startLb(View v) {
		startChannel(4796, false);// start Wikipedia EN
	}

	public void startChannel(int channelId, boolean andFinishActivity) {
		Intent intent = new Intent(SplashActivity.this, MetaioCloudARViewTestActivity.class);
		intent.putExtra(getPackageName() + ".CHANNELID", channelId);
		startActivity(intent);

		if (andFinishActivity)
			finish();
	}

	private class JunaioStarterTask extends AsyncTask<Integer, Integer, Integer> {

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(SplashActivity.this, "CouponArtV2", "Starting up...");
		}
 
		@Override
		protected Integer doInBackground(Integer... params) {

			// TODO Set authentication if a private channel is used
			// MetaioCloudPlugin.setAuthentication("username", "password");

			// Start junaio, this will initialize everything the plugin needs
			int result = MetaioCloudPlugin.startJunaio(this, getApplicationContext());

			return result;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {

		}

		@Override
		protected void onPostExecute(Integer result) {
			if (progressDialog != null) {
				progressDialog.cancel();
				progressDialog = null;
			}

			switch (result) {
			case MetaioCloudPlugin.ERROR_EXSTORAGE:
			case MetaioCloudPlugin.ERROR_INSTORAGE:
			case MetaioCloudPlugin.CANCELLED:
			case MetaioCloudPlugin.ERROR_CPU_NOT_SUPPORTED:
			case MetaioCloudPlugin.ERROR_GOOGLE_SERVICES:
				showDialog(result, null);
				break;
			case MetaioCloudPlugin.SUCCESS:
				launchLiveView();
				break;
			}
		}

	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {

		AlertDialog.Builder builder = new Builder(this);
		builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		switch (id) {
		case MetaioCloudPlugin.ERROR_EXSTORAGE:
			builder.setMessage("External storage is not available. If you have your USB plugged in, set the USB mode to only charge");
			break;
		case MetaioCloudPlugin.ERROR_INSTORAGE:
			builder.setMessage("Internal storage is not available");
			break;
		case MetaioCloudPlugin.CANCELLED:
			SplashActivity.log("Starting junaio cancelled");
			break;
		case MetaioCloudPlugin.ERROR_CPU_NOT_SUPPORTED:
			SplashActivity.log("CPU is not supported");
			break;
		case MetaioCloudPlugin.ERROR_GOOGLE_SERVICES:
			SplashActivity.log("Google APIs not found");
			break;
		}

		return builder.show();
	}

}
